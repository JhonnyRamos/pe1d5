// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyD7UrCGXy0Fdyp0G9bmwf_qoYORnuEbqqE',
    authDomain: 'controle-ifsp-gu3022226.firebaseapp.com',
    projectId: 'controle-ifsp-gu3022226',
    storageBucket: 'controle-ifsp-gu3022226.appspot.com',
    messagingSenderId: '299364083373',
    appId: '1:299364083373:web:f0871ebfc6082d9a040824',
    measurementId: 'G-T1VC6SLBQ1'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
