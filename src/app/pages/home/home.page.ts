import { BillService } from './../bill/service/bill.service';
import { Component } from '@angular/core';
import { LoginService } from '../auth/service/login.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  month  = new Date().toISOString()
  bill = {
    pay: {sum: 0, count: 0},
    receivable: {sum: 0, count: 0},
    balance: {sum: 0, count: 0},
  }

  constructor(
    private service: LoginService,
    private billService: BillService
  ) { }

    ionViewWillEnter() {
      this.showBills()
    }

    showBills() {
      this.bill.balance.sum = 0
      this.billService.sum('pay', this.month).then(x => {
        this.bill.pay = x
        this.bill.balance.sum -= x.sum
      })
      this.billService.sum('receivable', this.month).then(x => {
        this.bill.receivable = x
        this.bill.balance.sum += x.sum
      })
    }

    logout() {
      this.service.logout();
    }
}
