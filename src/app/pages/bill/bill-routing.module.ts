import { ListPage } from './list/list.page';
import { RegisterPage } from './register/register.page';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from './../../component/shared.module';
import { CommonModule } from '@angular/common';
import { ReportPage } from './report/report.page';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'pay',
        component: ListPage
      },
      {
        path: 'receivable',
        component: ListPage
      },
      {
        path: 'register',
        component: RegisterPage
      },
      {
        path: 'report',
        component: ReportPage
      }
    ]
  },
  {
    path: 'list',
    loadChildren: () => import('./list/list.module').then( m => m.ListPageModule)
  },
];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  declarations: [
    ListPage,
    RegisterPage,
    ReportPage
  ]
})
export class BillRoutingModule { }
