export class Bill {
    id: string
    partner: string
    description: string
    value: number
    type: String
}
