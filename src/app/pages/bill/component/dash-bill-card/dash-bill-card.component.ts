import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'dash-bill-card',
  templateUrl: './dash-bill-card.component.html',
  styleUrls: ['./dash-bill-card.component.scss'],
})
export class DashBillCardComponent implements OnInit {
  @Input() title: string;
  @Input() color: string;
  @Input() icon: string;
  @Input() type: string = "pay";
  @Input() billValue: string = "R$ 0,00";
  @Input() billSize: string = "0";

  constructor() { }

  ngOnInit() {
    switch (this.type) {
      case 'receivable':
          this.receivableBuild()
        break;
      case 'pay':
        this.payBuild()
        break;
      case 'balance':
        this.balanceBuild()
        break;
    }
  }

  receivableBuild() {
    this.title = this.title ? this.title : 'Contas à receber'
    this.icon = this.icon ? this.icon : 'arrow-down'
    this.color = this.color ? this.color : 'success'
  }

  payBuild() {
    this.title = this.title ? this.title : 'Contas à pagar'
    this.icon = this.icon ? this.icon : 'arrow-up'
    this.color = this.color ? this.color : 'danger'
  }

  balanceBuild() {
    this.title = this.title ? this.title : 'Saldo'
    this.icon = this.icon ? this.icon : 'wallet'
    this.color = this.color ? this.color : 'primary'
  }

  validView() {
    return ['pay', 'receivable'].includes(this.type)
  }
}
