import { DateHelper } from './../../../helpers/DateHelper';
import { NavController } from '@ionic/angular';
import { BillService } from './../service/bill.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  billForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private service: BillService,
    ) { }

  ngOnInit() {
    this.initForm()
  }

  create() {
    const data = this.billForm.value;
    const date = this.billForm.get('date').value
    delete data.date
    this.service.create({...data, ...DateHelper.transformDate(date)})
  }

  private initForm() {
    this.billForm = this.formBuilder.group({
      date: [new Date().toISOString(), Validators.required],
      partner: ['', [Validators.required, Validators.min(5)]],
      description: ['', [Validators.required, Validators.min(6)]],
      type: ['', [Validators.required]],
      value: ['', [Validators.required, Validators.min(0.01)]],
    });
  }
}
