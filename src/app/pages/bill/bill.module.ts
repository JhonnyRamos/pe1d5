import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { DashBillCardComponent } from './component/dash-bill-card/dash-bill-card.component';
import { SharedModule } from 'src/app/component/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BillRoutingModule } from './bill-routing.module';


@NgModule({
  declarations: [
    DashBillCardComponent,
  ],
  imports: [
    CommonModule,
    BillRoutingModule,
    IonicModule,
    RouterModule,
    SharedModule
  ],
  exports: [
    DashBillCardComponent,
  ]
})
export class BillModule { }
