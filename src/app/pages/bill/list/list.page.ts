import { Bill } from './../bill';
import { BillService } from './../service/bill.service';
import { AlertController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {
  bills: Bill[];
  type: string;

  constructor(
       private service: BillService,
       private alert: AlertController,
       private router: Router,
    ) { }

  ngOnInit() {
    this.type = this.router.url.split("/")[2]
  }

  ionViewWillEnter() {
    this.showBills()
  }

  showBills() {
    this.service.get(this.type).then(x => {
      const bills: any[] = []
      x.forEach((doc) => bills.push({
        id: doc.id,
        partner: doc.data().partner,
        description: doc.data().description,
        value: doc.data().value,
      }))
      this.bills = bills
     })
  }

  async remove(bill: Bill) {
    const confirm = await this.alert.create({
      header: "Remover conta",
      message: "Deseja apagar essa conta?",
      buttons: [
        {
          text: "Cancelar",
          role: "cancel"
        },
        {
          text: "Sim",
          handler: () => this.service.remove(bill)
        }
      ]
    })
    confirm.present()
  }

  async edit(bill: Bill) {
    const confirm = await this.alert.create({
      header: "Editar conta",
      inputs: [
        {
          name: "partner",
          value: bill.partner,
          placeholder: "Parceiro comercial",
        },
        {
          name: "description",
          value: bill.description,
          placeholder: "Descrição",
        },
        {
          name: "value",
          value: bill.value,
          type: "number",
          placeholder: "valor",
        },
      ],
      buttons: [
        {
          text: "Cancelar",
          role: "cancel"
        },
        {
          text: "Salvar",
          handler: (data) => {
            this.service.edit({...bill, ...data})
          }
        }
      ]
    })
    confirm.present()
  }
}
