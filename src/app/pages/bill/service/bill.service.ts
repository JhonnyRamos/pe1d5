import { DateHelper } from './../../../helpers/DateHelper';
import { Bill } from './../bill';
import { NavController, ToastController } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { Firestore } from '@angular/fire/firestore';
import { addDoc, collection, deleteDoc, doc, getDocs, query, updateDoc, where } from '@firebase/firestore';

@Injectable({
  providedIn: 'root'
})
export class BillService {
  constructor(
    private db: Firestore,
    private nav: NavController,
    private toastController: ToastController,
  ) { }

  create(data: Bill) {
    addDoc(collection(this.db, "bills"), data).then(() => {
      this.showToast("Conta cadastrada com sucesso.")
      this.nav.navigateForward(`bill/${data.type}`)
    }).catch(() => this.showToast("Falha ao cadastrar conta.", "error"));
  }

  //https://www.freecodecamp.org/news/the-firestore-tutorial-for-2020-learn-by-example/
  get(type: string) {
    return getDocs(query(collection(this.db, "bills"), where("type", "==", type)))
  }

  remove(bill: Bill) {
    deleteDoc(doc(this.db, "bills", bill.id))
  }

  edit(bill: Bill) {
    updateDoc(doc(this.db, "bills", bill.id), bill).then(() => {
      this.showToast("Conta editada com sucesso.")
      this.nav.navigateForward(`bill/${bill.type}`)
    }).catch(() => this.showToast("Falha ao editar conta.", "error"));
  }

  async sum(type: string, month: string) {
    try{
      const aux = DateHelper.transformDate(month)
      const data = await getDocs(query(collection(this.db, "bills"), 
        where("type", "==", type), 
        where("month", "==", aux.month), 
        where("year", "==", aux.year)
      ))
      let sum = 0
      let count = 0
      data.docs.forEach(snap => {
        const bill = snap.data()
        sum += parseFloat(bill.value)
        count++
      })
      return {sum: sum, count}
    } catch (error) {
      console.log(error)
      return {sum: 0, count: 0}
    }
  }

  private async showToast(message: string, color: string = "ifsp") {
    const toast = await this.toastController.create({
      message: message,
      color: color,
      duration: 3000
    });
    toast.present();
  }
}
