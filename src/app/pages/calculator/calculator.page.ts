import { CalculadoraService } from './../../services/calculadora.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'calculator',
  templateUrl: './calculator.page.html',
  styleUrls: ['./calculator.page.scss'],
})
export class CalculatorPage implements OnInit {
  title = "Calculadora";
  calculatorForm: FormGroup;
  quotient: number | string = 0;

  constructor(
    private formBuilder: FormBuilder,
    private service: CalculadoraService,
  ) { }

  ngOnInit() {
      this.calculatorForm = this.formBuilder.group({
        dividend: ['', Validators.required],
        divider: ['', Validators.required],
      })
  }

  divider() {
    const data = this.calculatorForm.value
    this.quotient = this.service.divide(data.dividend, data.divider)
  }
}
