import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CalculatorPage } from './calculator.page';
import { By } from '@angular/platform-browser';

describe('CalculatorPage', () => {
  let view: HTMLElement;
  let controller: CalculatorPage;
  let fixture: ComponentFixture<CalculatorPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CalculatorPage ],
      imports: [IonicModule.forRoot(), ReactiveFormsModule, FormsModule]
    }).compileComponents();

    fixture = TestBed.createComponent(CalculatorPage);
    controller = fixture.componentInstance;
    view = fixture.nativeElement
    fixture.detectChanges();
  }));

  it("should be has title", () => {
    expect(controller.title).toBeDefined()
  })

  it("should be showing title", () => {
    const result = view.querySelector(".title").textContent
    expect(result).toEqual('Calculadora')
  })

  it("should be disabled operator button", () => {
    expect(controller.calculatorForm.invalid).toBeTrue()
  })

  it("should be enabled operator button", () => {
    controller.calculatorForm.controls.dividend.setValue(0)
    controller.calculatorForm.controls.divider.setValue(0)
    expect(controller.calculatorForm.valid).toBeTrue()
  })

  it("should be can divider", () => {
    controller.calculatorForm.controls.dividend.setValue(10)
    controller.calculatorForm.controls.divider.setValue(2)

    const operator = fixture.debugElement.query(By.css("#calculate"))
    operator.triggerEventHandler('click', null)
    fixture.detectChanges()

    const result = view.querySelector("#quotient").textContent
    expect(result).toEqual('5')
  })

});
