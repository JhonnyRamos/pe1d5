import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'header-login',
  templateUrl: './header-login.component.html',
  styleUrls: ['./header-login.component.scss'],
})
export class HeaderLoginComponent implements OnInit {
  @Input() title: string = '';

  constructor() { }

  ngOnInit() {}

}
