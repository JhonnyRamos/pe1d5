import { Auth } from '@angular/fire/auth';
import { NavController, ToastController } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { createUserWithEmailAndPassword, sendPasswordResetEmail, signInWithEmailAndPassword, User } from '@firebase/auth';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  user: User;

  constructor(
    private nav: NavController,
    private auth: Auth,
    private toastController: ToastController,
  ) {
    // https://firebase.google.com/docs/auth/web/manage-users?hl=pt-br#web-version-9

    this.auth.onAuthStateChanged((user) => {
      this.user = user
      if (this.user) {
        this.nav.navigateForward('home');
      } else {
        console.log("Sem usuário");
      }
    })
  }

  login(user: { email: string; password: string; }) {
    signInWithEmailAndPassword(this.auth, user.email, user.password)
      .then(() => this.nav.navigateForward('home'))
      .catch((error) => {
        console.log(error)
        this.showToast("Dados de acesso incorretos.", "error");
      });
  }

  logout() {
    this.auth.signOut().finally(() => this.nav.navigateBack('auth'));
  }

  recoveryPass(data: { email: string}) {
    sendPasswordResetEmail(this.auth, data.email)
      .then(() => {
        this.nav.navigateBack('auth')
      })
      .catch((error) => {
        console.log(error)
        this.showToast("Email incorreto.", "error");
      })
  }

  createUser(user: { firstName: string, lastName: string, email: string, password: string, confirmPassword: string }) {
    createUserWithEmailAndPassword(this.auth, user.email, user.password)
      .then(credentials => console.log(credentials))
      .catch((error) => {
        console.log(error)
        this.showToast(`Occurred error in processing data.`, "error")
      });
  }

  private async showToast(message: string, color: string = "ifsp") {
    const toast = await this.toastController.create({
      message: message,
      color: color,
      duration: 3000
    });
    toast.present();
  }
}
