import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginPage } from './login/login.page';
import { ForgotPage } from './forgot/forgot.page';
import { RegisterPage } from './register/register.page';
import { SharedModule } from './../../component/shared.module';
import { HeaderLoginComponent } from './component/header-login/header-login.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: LoginPage },
      { path: 'register', component: RegisterPage },
      { path: 'forgot', component: ForgotPage }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [
    LoginPage,
    RegisterPage,
    ForgotPage,
    HeaderLoginComponent,
  ]
})
export class AuthRoutingModule { }
