import { RouterModule } from '@angular/router';
import { ButtonGoBackComponent } from './button-goback/button-goback.component';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


@NgModule({
  declarations: [
    ButtonGoBackComponent,
  ],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule,
  ],
  exports: [
    ButtonGoBackComponent,
  ],
})
export class SharedModule { }
