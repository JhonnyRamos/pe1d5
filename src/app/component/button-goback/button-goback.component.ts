import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'button-goback',
  templateUrl: './button-goback.component.html',
  styleUrls: ['./button-goback.component.scss'],
})
export class ButtonGoBackComponent implements OnInit {
  @Input() link: string = ''

  constructor() { }

  ngOnInit() {}

}
