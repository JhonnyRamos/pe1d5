import { TestBed } from '@angular/core/testing';

import { CalculadoraService } from './calculadora.service';

describe('CalculadoraService', () => {
  let service: CalculadoraService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CalculadoraService);
  });

  describe('should be divide any number', () =>{
    it('int numbers', () => {
      const result = service.divide(8, 4)
      expect(result).toBeTruthy()
      expect(result).toBe(2)
    })

    it('should be without divider 0', () => {
      const result = service.divide(8, 0)
      expect(typeof result).toEqual("string")
    })
  })
});
