import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CalculadoraService {

  constructor() { }

  divide(dividend: number, divider: number): number | string {
    if (divider != 0) {
      return dividend / divider
    }
    return "0 divisor"
  }
}
