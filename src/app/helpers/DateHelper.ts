export class DateHelper {
    static transformDate(date: string) {
        let aux: any[] = (date.split('T')[0]).split('-')
        aux = aux.map(x => {
          return parseInt(x)
        })
        return {year: aux[0], month: aux[1], day: aux[2]}
      }
}